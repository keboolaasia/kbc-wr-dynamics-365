import os
import pytest
import json
import csv
import requests

from wrdynamics.writer import main, create, update, delete, upsert
from wrdynamics.utils import verify_config, ConfigError
from dynamics365 import DynamicsClient
import logging
import random
import time

logging.basicConfig(level=logging.DEBUG)
UPDATE_ENTITY_ID = 'abd65c36-830d-e811-a833-000d3a80ebd6'
FAKE_ENTITY_ID = '00000000-0000-0000-0000-000000000007'


def common_config():
    return {
        "debug": True,
        "auth": {
            "client_id": os.getenv("WR_CLIENT_ID"),
            "#client_secret": os.getenv("WR_CLIENT_SECRET"),
            "redirect_uri": os.getenv("WR_REDIRECT_URI"),
            "resource": os.getenv("WR_RESOURCE"),
            "#refresh_token": os.getenv("WR_REFRESH_TOKEN")
        }
    }

def _prepare_intable(tmpdir, content, tablename='fake_input.csv'):
    intable = tmpdir.mkdir('in').mkdir('tables').join(tablename)
    intable.write(content)
    return tmpdir.strpath, tablename

@pytest.fixture(scope='module')
def api():
    client = DynamicsClient(**{
            "client_id": os.getenv("WR_CLIENT_ID"),
            "client_secret": os.getenv("WR_CLIENT_SECRET"),
            "redirect_uri": os.getenv("WR_REDIRECT_URI"),
            "resource": os.getenv("WR_RESOURCE"),
            "refresh_token": os.getenv("WR_REFRESH_TOKEN")
    })
    client._refresh_access_token()
    return client

def test_creating_entities_from_csv(tmpdir, api):
    now = int(time.time())
    nonce_a = str(now)
    nonce_b = str(now+1)
    content = """esp_name,esp_kbc_custom_field_1,esp_kbc_description
KEBOOLA WRITER,TO DELETE,{}
KEBOOLA WRITER B,TO DELETE,{}""".format(nonce_a, nonce_b)

    datadir, intable = _prepare_intable(tmpdir, content)
    config = [{
        'tablename': intable,
        'endpoint': 'esp_kbc_testings'
    }]

    with api:
        create(api, config, skip_errors=False, datadir=datadir)
        for nonce in [nonce_a, nonce_b]:
            created_row = api.get_one("esp_kbc_testings?$filter=esp_kbc_description eq '{}'".format(nonce))
            # clean up
            assert len(created_row['value']) == 1
            api.delete('esp_kbc_testings', created_row['value'][0]['esp_kbc_testingid'])

def test_updating_entities_from_csv(tmpdir, api):
    """
    There is a manually created entity in CRM for this test updating
    """
    now = int(time.time())
    nonce = str(now)
    content = """esp_kbc_custom_field_1,esp_kbc_testingid
{},{}""".format(nonce, UPDATE_ENTITY_ID)
    datadir, intable = _prepare_intable(tmpdir, content)
    config = [{
        'tablename': intable,
        'id_column': 'esp_kbc_testingid',
        'endpoint': 'esp_kbc_testings'
    }]

    with api:
        update(api, config, skip_errors=False, datadir=datadir)
        updated = api.get_one('esp_kbc_testings({})'.format(UPDATE_ENTITY_ID))
        assert updated['esp_kbc_custom_field_1'] == nonce

def test_updating_entities_with_invalid_id(tmpdir, api):
    nonce = str(int(time.time()))
    content = """esp_kbc_custom_field_1,esp_kbc_testingid
{},{}""".format(nonce, FAKE_ENTITY_ID)
    datadir, intable = _prepare_intable(tmpdir, content)
    config = [{
        'tablename': intable,
        'id_column': 'esp_kbc_testingid',
        'endpoint': 'esp_kbc_testings'
    }]

    with api:
        with pytest.raises(requests.HTTPError) as err:
            update(api, config, skip_errors=False, datadir=datadir)
            assert err.response.status_code == 404
        # nothing happens (except for logging)
        update(api, config, skip_errors=True, datadir=datadir)


def test_updating_entities_fails_on_invalid_field(tmpdir, api):
    now = int(time.time())
    nonce = str(now)
    content = """FAKE_FIELD_SHOULD_FAIL,esp_kbc_testingid
{},{}""".format(nonce, UPDATE_ENTITY_ID)
    datadir, intable = _prepare_intable(tmpdir, content)
    config = [{
        'tablename': intable,
        'id_column': 'esp_kbc_testingid',
        'endpoint': 'esp_kbc_testings'
    }]

    with api:
        with pytest.raises(requests.HTTPError) as err:
            update(api, config, skip_errors=False, datadir=datadir)
            assert err.response.status_code == 400

            update(api, config, skip_errors=False, datadir=datadir)
            assert err.response.status_code == 400


def test_deleting_entities(tmpdir, api):
    nonce = str(int(time.time()))
    with api:
        response = api.create('esp_kbc_testings',
                              json={'esp_name': "TO BE DELETED BY WRITER",
                                    'esp_kbc_custom_field_1': nonce,
                                    'esp_kbc_description': "custom_field_1 is unix timestamp nonce"})

        to_be_deleted = api.get_one("esp_kbc_testings?$filter=esp_kbc_custom_field_1 eq '{}'".format(nonce))

    content = """whatever_column,esp_kbc_testingid
anything,{}""".format(to_be_deleted['value'][0]['esp_kbc_testingid'])
    datadir, intable = _prepare_intable(tmpdir, content)
    config = [{
        'tablename': intable,
        'id_column': 'esp_kbc_testingid',
        'endpoint': 'esp_kbc_testings'
    }]

    with api:
        delete(api, config, skip_errors=False, datadir=datadir)
        shouldnt_exist = api.get_one("esp_kbc_testings?$filter=esp_kbc_custom_field_1 eq '{}'".format(nonce))
        assert len(shouldnt_exist['value']) == 0

def test_upserting(tmpdir, api):
    now = int(time.time())
    nonce = str(now)
    nonce_b = str(now + 1)
    content = """esp_kbc_custom_field_1,esp_kbc_testingid,esp_kbc_description
{},{},TEST UPSERT SHOULD BE UPDATED ALREADY EXISTING
{},{},TEST UPSERT SHOULD BE CREATED FROM SCRATCH""".format(nonce, UPDATE_ENTITY_ID,
                nonce_b, FAKE_ENTITY_ID)
    datadir, intable = _prepare_intable(tmpdir, content)
    config = [{
        'tablename': intable,
        'id_column': 'esp_kbc_testingid',
        'endpoint': 'esp_kbc_testings'
    }]

    with api:
        upsert(api, config, datadir=datadir)

        updated = api.get_one('esp_kbc_testings({})'.format(UPDATE_ENTITY_ID))
        assert updated['esp_kbc_custom_field_1'] == nonce

        # assert was created because was nonexistent

        fake_created = api.get_one('esp_kbc_testings({})'.format(FAKE_ENTITY_ID))
        assert updated['esp_kbc_custom_field_1'] == nonce
        # clean up
        api.delete('esp_kbc_testings', FAKE_ENTITY_ID)
