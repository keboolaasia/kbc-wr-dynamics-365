import pytest
import os
from wrdynamics.utils import serialize_input_table

def _prepare_intable(tmpdir, content, tablename='fake_input.csv'):
    intable = tmpdir.mkdir('in').mkdir('tables').join(tablename)
    intable.write(content)
    return tmpdir.strpath, tablename

def test_serializing_input_without_id_column(tmpdir):
    content = """test,idcolumn,value
foo,42,bar
fox,84,rab"""
    datadir, intable = _prepare_intable(tmpdir, content)
    config = {
            'tablename': intable,
            'endpoint': 'esp_kbc_testings'
        }

    rows = serialize_input_table(os.path.join(datadir, 'in/tables', intable))

    row_1 = next(rows)
    assert row_1 == {'test': 'foo', 'idcolumn':'42', 'value':'bar'}
    row_2 = next(rows)
    assert row_2 == {'test': 'fox', 'idcolumn':'84', 'value':'rab'}
    with pytest.raises(StopIteration):
        next(rows)
