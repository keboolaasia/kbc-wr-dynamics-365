import csv
import logging
REQUIRED_AUTH_FIELDS = ['client_id', '#client_secret', 'redirect_uri',
                        'resource', '#refresh_token']

POTENTIAL_ENDPOINT_CONFIGS = ["create"]
REQUIRED_ENDPOINT_FIELDS = ['tablename', 'endpoint']

# those require id_column
POTENTIAL_ENDPOINT_CONFIGS_ID = ["update", "update_skip_errors", "upsert", "delete", "create"]
REQUIRED_ENDPOINT_FIELDS_ID = ['tablename', 'endpoint', 'id_column']

class ConfigError(ValueError):
    """Invalid extractor configuration"""

def _verify_actions(params, root_configs, required_fields):
    errors = []
    for action in root_configs:
        try:
            action_cfg = params[action]
        except KeyError:
            # those are all mandatory
            continue
        else:
            for endpoint in action_cfg:
                for field in required_fields:
                    if field not in endpoint:
                        errors.append((
                            "action: '{action}' "
                            "endpoint: '{ep}' "
                            "field '{field}' "
                            "is missing and is required").format(
                            action=action,
                            ep=endpoint,
                            field=field))
    return errors

def verify_config(params):
    # check auth data is there

    errors = []
    try:
        auth = params['auth']
    except KeyError:
        errors.append('Provide "auth" data')
    else:
        for field in REQUIRED_AUTH_FIELDS:
            if field not in auth:
                errors.append("auth.{} is missing!".format(field))

    errors += _verify_actions(params, POTENTIAL_ENDPOINT_CONFIGS, REQUIRED_ENDPOINT_FIELDS)
    errors += _verify_actions(params, POTENTIAL_ENDPOINT_CONFIGS_ID, REQUIRED_ENDPOINT_FIELDS_ID)

    for error in errors:
        logging.error(error)
    if errors:
        raise ConfigError("Check above errors in config.json and fix them.")


def serialize_input_table(tablename):
    """
    Assume config is valid
    """
    with open(tablename) as fin:
        data = csv.DictReader(fin)
        for row in data:
            yield row
