"""
KBC Writer
"""
import logging
import os
import csv
import json
import re
import requests
from dynamics365 import DynamicsClient
from .utils import serialize_input_table
import logging



# Each processing function takes path to csv

def _create_one(api, config, skip_errors, datadir):
    logging.info("Creating '%s'", config)
    intable = os.path.join(datadir, 'in/tables/', config['tablename'])
    for row_data in serialize_input_table(intable):
        try:
            api.create(config['endpoint'], json=row_data)
        except requests.HTTPError as err:
            if skip_errors and err.response.status_code in {404}:
                logging.error("Creating '%s' failed. Skipping since skip_errors=True", row_data)
            else:
                logging.error(err.response.text)
                raise

def create(api, config, skip_errors, datadir):
    """
    Create enntities as descibed by config
    1. parse the input csv
    2. send requests to CRM (1 row == 1 request) (async here?)

    Args:
        config (list): a list like this [{
                "endpoint": "accounts",
                "tablename": "accounts_to_create.csv"}]
        skip_errors (bool): if True continues on 404

    """
    logging.info("Creating entities; skip_errors={}".format(skip_errors))
    for endpoint in config:
        _create_one(api, endpoint, skip_errors, datadir)

def _update_one(api, config, skip_errors, datadir):
    """
    Update all records for one entity

    Args:
        config (dict): with keys 'id_column', 'endpoint' and 'tablename'
    """
    logging.info("Updating '%s'", config)
    id_column = config['id_column']
    intable = os.path.join(datadir, 'in/tables/', config['tablename'])
    for row_data in serialize_input_table(intable):
        entity_id = row_data.pop(id_column)
        try:
            resp = api.update(config['endpoint'], entity_id, json=row_data)
        except requests.HTTPError as err:
            if skip_errors and err.response.status_code in {404}:
                logging.warning(
                    "'%s'='%s' Doesn't exist. Skipping since skip_errors=True",
                    id_column, entity_id)
            else:
                logging.error(err.response.text)
                raise

def update(api, config, skip_errors, datadir):
    """
    Update entities as described by config
    """
    logging.info("Updating entities; skip_errors={}".format(skip_errors))
    for entity in config:
        _update_one(api, entity, skip_errors, datadir)

def _delete_one(api, config, skip_errors, datadir):
    """
    Delete all records for one entity

    Args:
        config (dict): with keys 'id_column', 'endpoint' and 'tablename'
    """
    logging.info("Deleting '%s'", config)
    id_column = config['id_column']
    intable = os.path.join(datadir, 'in/tables/', config['tablename'])
    for row_data in serialize_input_table(intable):
        try:
            api.delete(config['endpoint'], row_data[id_column])
        except requests.HTTPError as err:
            if skip_errors and err.response.status_code in {404}:
                logging.warning(
                    "'%s'='%s' Doesn't exist. Skipping since skip_errors=True",
                    id_column, row_data[id_column])
            else:
                logging.error(err.response.text)
                raise
def delete(api, config, skip_errors, datadir):
    logging.info("Deleting entities; skip_errors={}".format(skip_errors))
    for entity in config:
        _delete_one(api, entity, skip_errors, datadir)



def _upsert_one(api, config, datadir):
    """
    Upsert all records for one entity

    """
    logging.info("Upserting '%s'", config)
    id_column = config['id_column']
    intable = os.path.join(datadir, 'in/tables/', config['tablename'])
    for row_data in serialize_input_table(intable):
        entity_id = row_data.pop(id_column)
        try:
            resp = api.upsert(config['endpoint'], entity_id, json=row_data)
        except requests.HTTPError as err:
            logging.error(err.response.text)
            raise

def upsert(api, config, datadir):
    logging.info("Upserting entities")
    for entity in config:
        _upsert_one(api, entity, datadir)

def main(params, datadir):
    """Run the extractor"""

    outdir = os.path.join(datadir, 'out/tables')
    try:
        os.makedirs(outdir)
    except FileExistsError:
        logging.debug(outdir + " already exists")

    auth = params['auth']
    api = DynamicsClient(client_id=auth['client_id'],
                         client_secret=auth['#client_secret'],
                         refresh_token=auth['#refresh_token'],
                         resource=auth['resource'],
                         redirect_uri=auth['redirect_uri'])


    create_config = params.get('create')
    if create_config:
        with api:
            create(api, create_config, skip_errors=False, datadir=datadir)

    update_config = params.get('update')
    if update_config:
        update(api, update_config, skip_errors=False, datadir=datadir)

    update_skip_errors_config = params.get('update_skip_errors')
    if update_skip_errors_config:
        update(api, update_skip_errors_config, skip_errors=True, datadir=datadir)

    # parse update_one_property

    delete_config = params.get('delete')
    if delete_config:
        delete(api, delete_config, skip_errors=False, datadir=datadir)

    delete_skip_errors_config = params.get('delete_skip_errors')
    if delete_skip_errors_config:
        delete(api, delete_skip_errors_config, skip_errors=True, datadir=datadir)

    upsert_config = params.get('upsert')
    if upsert_config:
        upsert(api, upsert_config, datadir)
