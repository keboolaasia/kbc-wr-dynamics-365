#!/bin/env python3
import logging
import sys
import os
import traceback
import requests
from keboola import docker
from wrdynamics import main
from wrdynamics.utils import verify_config


if __name__ == "__main__":
    try:
        datadir = os.getenv('KBC_DATADIR')
        cfg = docker.Config(data_dir=datadir)
        params = cfg.get_parameters()

        if params.get('debug'):
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

        verify_config(params)

        main(params, datadir)
    except (ValueError, KeyError) as err:
        logging.exception("Something wrong with the input")
        sys.exit(1)
    except requests.HTTPError as err:
        if 400 <= err.response.status_code < 499:
            logging.exception(
                "Wrong request. This could be a malformed endpoint or "
                "insufficient permissions:")
            sys.exit(1)
        else:
            sys.exit(2)
    except Exception as err:
        logging.exception("App error")
        sys.exit(2)
